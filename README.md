[![build status](https://gitlab.com/zugvogel/oxid-php-fpm/badges/main/pipeline.svg)](https://gitlab.com/zugvogel/oxid-php-fpm/commits/main)
[![coverage report](https://gitlab.com/zugvogel/oxid-php-fpm/badges/main/coverage.svg)](https://gitlab.com/zugvogel/oxid-php-fpm/commits/main)

# Docker Container Name

oxid-php-fpm is a docker container providing PHP-FPM with all modules required to run
an [OXID eShop](http://www.oxid-esales.com/).

## Getting Started

These instructions will cover usage information and for the docker container.

### Prerequisities

In order to run this container you'll need docker installed.

* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)

### Usage

#### Container Parameters

To start the container use

```shell
docker run -d -v "$PWD":/var/www/html registry.gitlab.com/zugvogel/oxid-php-fpm:8.1.11
```

To start a shell within the container use

```shell
docker run -it registry.gitlab.com/zugvogel/oxid-php-fpm:8.1.11 bash
```

#### Environment Variables

See [base image](https://hub.docker.com/_/php/).

#### Volumes

See [base image](https://hub.docker.com/_/php/).

#### Useful File Locations

See [base image](https://hub.docker.com/_/php/).

## Built With

* PHP 8.1.11

## Find Us

* [GitLab Repository](https://gitlab.com/zugvogel/oxid-php-fpm)
* [GitLab Registry](https://gitlab.com/zugvogel/oxid-php-fpm/container_registry)

## Versioning

We use the base images version.

## Authors

* **Julian Nodorp** - *Initial work* - [jnodorp](https://gitlab.com/jnodorp)

See also the list of [contributors](https://gitlab.com/zugvogel/oxid-php-fpm/graphs/main) who 
participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
