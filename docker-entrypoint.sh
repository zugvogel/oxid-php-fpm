#!/bin/sh

echo "Changing UID for www-data to $USER_ID"
usermod -u $USER_ID www-data

exec "$@"
