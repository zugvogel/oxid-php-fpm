FROM php:8.2-fpm
LABEL org.opencontainers.image.authors Julian Nodorp <julian.nodorp@zugvogel.shop>

# Install packages and PHP extensions required by OXID eShop.
# Exception: libfcgi-bin is required for Kubernetes probes.
RUN apt-get update \
 && apt-get install -y --no-install-recommends libjpeg62-turbo-dev \
                                               libpng-dev \
                                               libfreetype6-dev \
                                               libxml2-dev \
                                               libonig-dev \
                                               libfcgi-bin \
 && rm -rf /var/lib/apt/lists/* \
 && docker-php-ext-configure gd --with-freetype --with-jpeg \
 && docker-php-ext-install -j$(nproc) gd \
                                      mbstring \
                                      iconv \
                                      bcmath \
                                      soap \
                                      pdo \
                                      pdo_mysql

# Enable status and ping pages for Kubernetes probes.
RUN sed -i 's/;pm.status_path = \/status/pm.status_path = \/status/' /usr/local/etc/php-fpm.d/www.conf \
 && sed -i 's/;ping.path = \/ping/ping.path = \/ping/' /usr/local/etc/php-fpm.d/www.conf

COPY docker-entrypoint.sh /usr/local/bin
ENTRYPOINT [ "docker-entrypoint.sh" ]
CMD [ "php-fpm" ]
